-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 26, 2020 at 04:10 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `youtube`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE `tbl_category` (
  `cid` int(11) NOT NULL,
  `category_name` varchar(255) NOT NULL,
  `category_image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`cid`, `category_name`, `category_image`) VALUES
(6, 'Mizanur Rahman Azhari waz', '4665-2020-06-26.png'),
(7, 'Motivational Speech', '1782-2020-06-26.png'),
(8, 'মিজানুর রহমান আজহারী নতুন ওয়াজ', '6414-2020-06-26.jpg'),
(9, 'Molla najim uddin video watch', '5623-2020-06-26.jpg'),
(10, 'test', '2889-2020-06-26.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fcm_template`
--

CREATE TABLE `tbl_fcm_template` (
  `id` int(11) NOT NULL,
  `message` text NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_fcm_template`
--

INSERT INTO `tbl_fcm_template` (`id`, `message`, `image`) VALUES
(27, 'Push Notification Test', ''),
(28, 'Hello World, This is Your Videos Channel App!!', '7843-2017-04-11.jpg'),
(29, 'This is Your Videos Channel App', ''),
(30, 'Hello Users', '5397-2020-06-26.png');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fcm_token`
--

CREATE TABLE `tbl_fcm_token` (
  `id` int(11) NOT NULL,
  `token` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_fcm_token`
--

INSERT INTO `tbl_fcm_token` (`id`, `token`) VALUES
(1, 'dffHw881NLc:APA91bEsmMBPN8RnIR_7ZsAmof6ydxOrxx-cPI0CvDqvuWK5XfLW-UGx9Zmf9XOqD14dv1o-5UOFfDd2uThnYZnq8XWFu4vCVn-ibMBokVu8Rds4xZOXGq5b6X44sqizULlI7QsMfFH0'),
(2, 'eJAiS-R8lQ0:APA91bHAmYkAnxV8rYz-7cxYVTaA3XF9MOfUam262QvSeGbjxUzT6nWJfXvTLaooy2lptL1X4glYTsnKKhazhmlBR3oyvX_GoKU-H7hnq9rjI-PmAsjhtGCj7i4nSk-DL37JT2crsllu'),
(3, 'ceoeJJDtdy4:APA91bGD1SwaDYme_cVV-kOVD3rja1JQ_nDemqkN0nP9u1Iet825p0BkzIxgcB6KvN792L4080W-Bh7_ctybtvOYYHHUIwGnKCzSNLm8-T0x8JNWqXyI61tbK993TM-trF2lmfYqlBWY'),
(4, 'fhri8oCZhFQ:APA91bGhicsmAAnFZh9qxNPIEZT8cOuOf_P3UfmevAn8DBcEHZTVBjoWEcsLIx8IoX6nMBvbd3AFC2TsSvn1J7kpJMXuPiZIwM5QRFJso4IrLu350mgNqsV5cybj1e03J783HLOxQDqQ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_gallery`
--

CREATE TABLE `tbl_gallery` (
  `id` int(11) NOT NULL,
  `cat_id` int(11) NOT NULL,
  `video_title` varchar(255) NOT NULL,
  `video_url` varchar(500) NOT NULL,
  `video_id` varchar(255) NOT NULL,
  `video_thumbnail` varchar(255) NOT NULL,
  `video_duration` varchar(255) NOT NULL,
  `video_description` text NOT NULL,
  `video_type` varchar(45) NOT NULL,
  `size` varchar(255) NOT NULL,
  `total_views` int(11) NOT NULL DEFAULT '0',
  `date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_gallery`
--

INSERT INTO `tbl_gallery` (`id`, `cat_id`, `video_title`, `video_url`, `video_id`, `video_thumbnail`, `video_duration`, `video_description`, `video_type`, `size`, `total_views`, `date_time`) VALUES
(10, 6, 'আজহারী হুজুরের সেরা ওয়াজ | New Waz 2020 | Mizanur Rahman Azhari new waz | Was | Waj | Was bangla', 'https://www.youtube.com/watch?v=k4vm4HASsJo', 'k4vm4HASsJo', '', '1:11:40', '<p>Watch Mizanur Rahman Azhari Bangla Waz &ldquo;New Waz 2020 &ndash; নতুন ওয়াজ ২০২০&rdquo;. Watch Islamic bangla waz mahfil and get any kind of life solution from islamic waz. In this video of Maulana Mizanur Rahman Azhari you can find the topics - waj, bangla waz, mizanur rahman azhari new waz, bangla waz 2019, mizanur rahman azhari, bangladeshi waz, waz, waz bangla, watch, mizanur rahman azhari waz, owaz, wsj 2019, oaj, new waz, woaj &amp; notun waz. You can also find the topics oaj new 2019, was was, waz 2019, bangla waz mizanur rahman azhari, waz bangla 2019, new waz 2019, bangla new waz &amp; bangla jalsa.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>বিষয়ঃ নতুন ওয়াজ ২০২০ বয়ানেঃ মাওলানা মিজানুর রহমান আজহারী।</p>\r\n\r\n<p>Lecturer: Mizanur Rahman Azhari Subscribe Now: <a href=\\\"https://www.youtube.com/channel/UCDik9UsvvanFZkNd-x1L48Q?sub_confirmation=1\\\">https://www.youtube.com/channel/UCDik...</a></p>\r\n\r\n<p>▦ মাওলানা মিজানুর রহমান আজহারী হুজুরের শ্রেষ্ঠ ওয়াজগুলো: ▦</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>►নবীজি নিরক্ষর ছিলেন!! মিজানুর রহমান আজহারী, কী বুঝিয়েছেন??: <a href=\\\"https://www.youtube.com/watch?v=TlzKh-beUQI\\\">https://youtu.be/TlzKh-beUQI</a></p>\r\n\r\n<p>►কিয়ামতের ভয়াবহতা ! মিজানুর রহমান আজহারী ওয়াজ ২০১৯: <a href=\\\"https://www.youtube.com/watch?v=ICWI4uzxIJU\\\">https://youtu.be/ICWI4uzxIJU</a></p>\r\n\r\n<p>►মুমিনের ৭ দফা !! মিজানুর রহমান আজহারী নতুন ওয়াজ ২০১৯: <a href=\\\"https://www.youtube.com/watch?v=6AgThiCBjxs\\\">https://youtu.be/6AgThiCBjxs</a></p>\r\n\r\n<p>►খুবই ফজিলতপূর্ণ &#39;সুরা ইয়াসিন&#39; তাফসীর-Mizanur Rahman Azhari waz: <a href=\\\"https://www.youtube.com/watch?v=lnWK4p-T6Cg\\\">https://youtu.be/lnWK4p-T6Cg</a></p>\r\n\r\n<p>►Mizanur Rahman Azhari new bangla waz 2019 | মোটিভেশনাল সুরা: <a href=\\\"https://www.youtube.com/watch?v=vViPtBmJ-ss\\\">https://youtu.be/vViPtBmJ-ss</a></p>\r\n\r\n<p>►যে সুরা পড়ে ফুঁ দিলে রোগী সুস্থ হয়ে যায় !!: <a href=\\\"https://www.youtube.com/watch?v=1OT3PbaL-UM\\\">https://youtu.be/1OT3PbaL-UM</a></p>\r\n\r\n<p>►কিয়ামত কখন হবে? : <a href=\\\"https://www.youtube.com/watch?v=DRC-_uBYqUY\\\">https://youtu.be/DRC-_uBYqUY</a></p>\r\n\r\n<p>►আজহারী হুজুরের শেষ ওয়াজ ২০১৯: <a href=\\\"https://www.youtube.com/watch?v=56McYEyt_AM\\\">https://youtu.be/56McYEyt_AM</a></p>\r\n\r\n<p>►নবীজি কি নিরক্ষর ছিলেন?: <a href=\\\"https://www.youtube.com/watch?v=rlow6uIAKsA\\\">https://youtu.be/rlow6uIAKsA</a></p>\r\n', 'youtube', '', 5, '2020-06-26 08:08:49'),
(11, 8, 'সত্যিকার ভালোবাসা ! মিজানুর রহমান আজহারী নতুন ওয়াজ | Mizanur Rahman Azhari new waz 2020 | Bangla Waz', 'https://www.youtube.com/watch?v=AW41ESukgXg', 'AW41ESukgXg', '', '22.20', '<p>&nbsp;</p>\r\n\r\n<p>Watch Mizanur Rahman Azhari Bangla Waz 2019 &ldquo;Bhalobasha Kare Koi &ndash; ভালোবাসা কারে কয়&rdquo;. Watch Islamic bangla waz mahfil and get any kind of life solution from islamic waz. Islam is the complete code of life.</p>\r\n\r\n<p>বিষয়: ভালোবাসা দিবসে আজহারী ভক্তদের জন্য উপহার &#39;ভালোবাসা কারে কয়&#39;</p>\r\n\r\n<p>বয়ানে: মাওলানা মিজানুর রহমান আজহারী।</p>\r\n\r\n<p>স্থান: ফরিদপুর</p>\r\n\r\n<p>Lecturer: Maulana Mizanur Rahman Azhari</p>\r\n\r\n<p>Islamic Waz: Bhalobasha Kare Koi</p>\r\n\r\n<p>Audio Copyright: Nice Waz</p>\r\n\r\n<p>Video Copyright: Nice Waz</p>\r\n\r\n<p>Waz Recorded by Nice Waz</p>\r\n', 'youtube', '', 3, '2020-06-26 08:59:34'),
(13, 8, 'হযরত ওমরের জীবন কাহিনী | মিজানুর রহমান আজহারী | Mizanur Rahman Azhari new waz | Azhari Waz 2020', 'https://www.youtube.com/watch?v=x-I3j5xX0tQ', 'x-I3j5xX0tQ', '', '1.10.05', '<p>&nbsp;</p>\r\n\r\n<p>Watch Mizanur Rahman Azhari Bangla Waz 2020 &ldquo;New waz 2020 &ndash; নতুন ওয়াজ ২০২০&rdquo;. Watch Islamic bangla waz mahfil and get any kind of life solution from islamic waz. Islam is the complete code of life. In this waj mahfil video Mizanur Rahman Azhari tell the story of Hazrat Omar, the great leader of Islamic History. In this video waz of Maulana Mizanur Rahman Azhari you can find the topics- waz, bangla waz, mizanur rahman azhari, waz bangla, মিজানুর রহমান আজহারী নতুন ওয়াজ, azhari waz, mizanur rahman azhari new waz, ওয়াজ, azhari waz 2020 &amp; mizanur rahman azhari waz. You can also find the topics- mijanur rahman ajhari new waz, waz mizanur rahman azhari, মিজানুর রহমান আজহারী, ওয়াজ মিজানুর রহমান আজহারী, mijanur rahman ajhari, bangla waz mizanur rahman azhari, bangla waz 2020 &amp; new waz 2020.</p>\r\n\r\n<p>বিষয়ঃ হযরত ওমরের জীবন কাহিনী</p>\r\n\r\n<p>বক্তা: মাওলানা মিজানুর রহমান আজহারী / Maulana Mizanur Rahman Azhari</p>\r\n\r\n<p>স্থান: ফরিদপুর</p>\r\n', 'youtube', '', 0, '2020-06-26 09:10:56'),
(14, 6, 'মোল্লা নাজিম উদ্দিন নতুন বাংলা ওয়াজ ২০১৮ - Molla Nazim Uddin New Bangla Waz 2018 - Islamic Life', 'https://www.youtube.com/watch?v=sWfD94sD5o4', 'sWfD94sD5o4', '', '1:20:36', '', 'youtube', '', 2, '2020-06-26 09:21:18'),
(15, 6, 'iOS 14 Hands-On: Everything New!', 'https://www.youtube.com/watch?v=ZLyDvABxGF0', 'ZLyDvABxGF0', '', '10:20', '', 'youtube', '', 1, '2020-06-26 09:43:30'),
(17, 10, 'Sami Yusuf - Hasbi Rabbi (Live in New Delhi, INDIA)', 'https://www.youtube.com/watch?v=1xY6Jomnw30', '1xY6Jomnw30', '', '03:17', '<p>Download/Stream &lsquo;Hasbi Rabbi (Live in New Delhi, INDIA)&#39; <a href=\\\"https://www.youtube.com/redirect?v=1xY6Jomnw30&amp;event=video_description&amp;redir_token=mgIkpBa7dZGlAG3baiudD4iyK3B8MTU5MzI1NTIyNkAxNTkzMTY4ODI2&amp;q=https%3A%2F%2Fsy.lnk.to%2FHasbiRabbiLiveInNewDelhi\\\" target=\\\"_blank\\\">https://sy.lnk.to/HasbiRabbiLiveInNew...</a> Watch Sami&#39;s Official YouTube Playlist <a href=\\\"https://www.youtube.com/redirect?v=1xY6Jomnw30&amp;event=video_description&amp;redir_token=mgIkpBa7dZGlAG3baiudD4iyK3B8MTU5MzI1NTIyNkAxNTkzMTY4ODI2&amp;q=http%3A%2F%2Fandnt.co%2FSYplaylist\\\" target=\\\"_blank\\\">http://andnt.co/SYplaylist</a> &ndash; Join Sami Yusuf on @Spotify and add your favourite tracks to your personal playlist <a href=\\\"https://www.youtube.com/redirect?v=1xY6Jomnw30&amp;event=video_description&amp;redir_token=mgIkpBa7dZGlAG3baiudD4iyK3B8MTU5MzI1NTIyNkAxNTkzMTY4ODI2&amp;q=http%3A%2F%2Fandnt.co%2FSY-Spotify\\\" target=\\\"_blank\\\">http://andnt.co/SY-Spotify</a> &ndash; Subscribe to Sami&#39;s official YouTube channel <a href=\\\"https://www.youtube.com/redirect?v=1xY6Jomnw30&amp;event=video_description&amp;redir_token=mgIkpBa7dZGlAG3baiudD4iyK3B8MTU5MzI1NTIyNkAxNTkzMTY4ODI2&amp;q=http%3A%2F%2Fandnt.co%2FYT-samiyusuf\\\" target=\\\"_blank\\\">http://andnt.co/YT-samiyusuf</a> Follow Sami Yusuf on: <a href=\\\"https://www.youtube.com/redirect?v=1xY6Jomnw30&amp;event=video_description&amp;redir_token=mgIkpBa7dZGlAG3baiudD4iyK3B8MTU5MzI1NTIyNkAxNTkzMTY4ODI2&amp;q=http%3A%2F%2Fwww.twitter.com%2Fsamiyusuf\\\" target=\\\"_blank\\\">http://www.twitter.com/samiyusuf</a> <a href=\\\"https://www.youtube.com/redirect?v=1xY6Jomnw30&amp;event=video_description&amp;redir_token=mgIkpBa7dZGlAG3baiudD4iyK3B8MTU5MzI1NTIyNkAxNTkzMTY4ODI2&amp;q=http%3A%2F%2Fwww.facebook.com%2Fsamiyusuf\\\" target=\\\"_blank\\\">http://www.facebook.com/samiyusuf</a> <a href=\\\"https://www.youtube.com/redirect?v=1xY6Jomnw30&amp;event=video_description&amp;redir_token=mgIkpBa7dZGlAG3baiudD4iyK3B8MTU5MzI1NTIyNkAxNTkzMTY4ODI2&amp;q=http%3A%2F%2Fwww.instagram.com%2Fsamiyusuf\\\" target=\\\"_blank\\\">http://www.instagram.com/samiyusuf</a> <a href=\\\"https://www.youtube.com/redirect?v=1xY6Jomnw30&amp;event=video_description&amp;redir_token=mgIkpBa7dZGlAG3baiudD4iyK3B8MTU5MzI1NTIyNkAxNTkzMTY4ODI2&amp;q=http%3A%2F%2Fwww.samiyusufofficial.com\\\" target=\\\"_blank\\\">http://www.samiyusufofficial.com</a> Performed live on 9th February 2019 at The Sufi Route (New Delhi, India) Special thanks to The Sufi Route. Performed and arranged by Sami Yusuf Music &amp; Words: Traditional (Afghani Folklore) Additional Words: Mehboob (Urdu); B. Radmanesh (Azeri). Mixed &amp; Mastered by Vishnu Rajan @ Andante Studios Video editing and post-production @ Andante Studios Translation: [Urdu:] Who is the only One? Who is the King? Who is the Merciful? Who is the most praised and benevolent? Whatever you see in this world is His sign He&rsquo;s the love of every soul CHORUS [Arabic:] My Lord is enough for me, Glory be to God There is nothing in my heart except God The light of Mohammed, God&rsquo;s peace and blessings be upon him There is none worthy of worship but God [Turkish] He is the Forgiver of all sins He is the King of the universe He is the Refuge of all hearts O God hear my sorrows and my sighs Have mercy and pardon my sins Bless my night and days CHORUS [Arabic:] O Lord of the worlds Send peace and blessings On Taha the trustworthy In every time and at every instant Fill my heart with conviction Make me steadfast on this way of life And forgive me and all the believers My Lord is enough for me, Glory be to God There is nothing in my heart except God The light of Mohammed, God&rsquo;s peace and blessings be upon him There is none worthy of worship but God Published by Andante Records &amp; Administered by Fairwood Music (UK) Ltd for the World Copyright of Andante Records. All rights reserved.</p>\r\n', 'youtube', '', 2, '2020-06-26 10:54:28');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_settings`
--

CREATE TABLE `tbl_settings` (
  `id` int(11) NOT NULL,
  `app_fcm_key` text NOT NULL,
  `api_key` varchar(255) NOT NULL,
  `privacy_policy` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_settings`
--

INSERT INTO `tbl_settings` (`id`, `app_fcm_key`, `api_key`, `privacy_policy`) VALUES
(1, 'AAAAIeURbfQ:APA91bHoo0AbFWHcT-gsvU6rHaWx0Fm9Snq6pByyHarF96J0QCDdzz--2NGFJyMi2EL2uobiLps_9kOrFNnayVYRwYah4naqTkz4GkCn2yn68Jp-6qwzhoski2OalxeESIPz8EEu-DYc', 'cda11EXOLIYleZ5CQBJidFh8RSMsmUKgfDT91cjH0oq7xParGv', '<h2><strong>Privacy Policy</strong></h2>\r\n\r\n<p>This privacy policy includes all the details about the data collected in Your Videos Channel and how it&rsquo;s used.</p>\r\n\r\n<p>&nbsp;</p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `user_role` enum('100','101','102') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `email`, `user_role`) VALUES
(1, 'admin', 'ac0e7d037817094e9e0b4441f9bae3209d67b02fa484917065f71b16109a1a78', 'fsfoysal15@gmail.com', '100');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_category`
--
ALTER TABLE `tbl_category`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `tbl_fcm_template`
--
ALTER TABLE `tbl_fcm_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_fcm_token`
--
ALTER TABLE `tbl_fcm_token`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_category`
--
ALTER TABLE `tbl_category`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_fcm_template`
--
ALTER TABLE `tbl_fcm_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tbl_fcm_token`
--
ALTER TABLE `tbl_fcm_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_gallery`
--
ALTER TABLE `tbl_gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_settings`
--
ALTER TABLE `tbl_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
